<?php declare (strict_types=1);

namespace Consortia;

/**
 * Time taken 40 min.
 */
class SortStringToVowels
{
    private const VOWELS_PATTERN = '/[aeiou]/i';

    public function sortStringsByVowels(array $array): array
    {
        $sortByVowels = function(string $subject, string $nextSubject) {
            $subjectVowelMatches = [];
            $nextSubjectVowelMatches = [];

            preg_match_all(self::VOWELS_PATTERN, $subject, $subjectVowelMatches);
            preg_match_all(self::VOWELS_PATTERN, $nextSubject, $nextSubjectVowelMatches);

            $subjectVowels = implode('', reset($subjectVowelMatches));
            $nextSubjectVowels = implode('', reset($nextSubjectVowelMatches));

            $subjectVowelsLength = strlen($subjectVowels);
            $nextSubjectVowelLength = strlen($nextSubjectVowels);

            if ($subjectVowelsLength === $nextSubjectVowelLength) {
                return 0;
            }

            return $subjectVowelsLength < $nextSubjectVowelLength;
        };

        usort($array, $sortByVowels);

        return $array;
    }
}
