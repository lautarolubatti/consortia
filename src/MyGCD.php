<?php declare (strict_types=1);

namespace Consortia;

/**
 * Time taken 5 min.
 */
class MyGCD
{
    /**
     * Greatest common divisor based on Euclidean algorithm
     *
     * @see https://en.wikipedia.org/wiki/Euclidean_algorithm
     */
    public function myGCD(int $x, int $y): int
    {
        return ($x % $y) ? $this->myGCD($y,$x % $y) : $y;
    }
}
