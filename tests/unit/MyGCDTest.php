<?php declare (strict_types=1);

namespace Consortia\Tests\Unit;

use Consortia\MyGCD;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Consortia\MyGCD
 */
class MyGCDTest extends TestCase
{
    /**
     * Subject under test
     */
    private MyGCD $sut;

    public function setUp(): void
    {
        $this->sut = new MyGCD();
    }

    public function test()
    {
        $this->assertSame(1, $this->sut->myGCD(1,3));
        $this->assertSame(12, $this->sut->myGCD(60,12));
        $this->assertSame(334, $this->sut->myGCD(2672,5678));
        $this->assertSame(846, $this->sut->myGCD(10927782,6902514));
        $this->assertSame(4, $this->sut->myGCD(1590771464,1590771620));
    }
}
