<?php declare (strict_types=1);

namespace Consortia\Tests\Unit;

use Consortia\SortStringToVowels;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Consortia\SortStringToVowels
 */
class SortStringToVowelsTest extends TestCase
{
    /**
     * Subject under test
     */
    private SortStringToVowels $sut;

    public function setUp(): void
    {
        $this->sut = new SortStringToVowels();
    }

    public function test()
    {
        $this->assertEquals(
            ["iiii","eee","aa","oo"],
            $this->sut->sortStringsByVowels(["aa","eee","oo","iiii"])
        );

        $this->assertEquals(
            ["ooo","ii","a","e","u"],
            $this->sut->sortStringsByVowels(["a","e","ii","ooo","u"])
        );

        $this->assertEquals(
            ["uoiea","ioue","ee"],
            $this->sut->sortStringsByVowels(["ioue","ee","uoiea"])
        );

        $this->assertEquals(
            ["boot","high","day"],
            $this->sut->sortStringsByVowels(["high","day","boot"])
        );

        $this->assertEquals(
            ["a beautiful trio of","how about now"],
            $this->sut->sortStringsByVowels(["how about now","a beautiful trio of"])
        );

        $this->assertEquals(
            ["its a beautiful day","yes, but a bit breezy"],
            $this->sut->sortStringsByVowels(["its a beautiful day","yes, but a bit breezy"])
        );
    }
}
